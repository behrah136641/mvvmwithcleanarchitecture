package com.behrah.mvvmwithcleanarchitecture.extentions

import android.graphics.Typeface
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import com.behrah.domain.entities.UserEntity
import com.behrah.mvvmwithcleanarchitecture.entities.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext


fun <T> LifecycleOwner.observe(liveData: LiveData<T>, body: (T) -> Unit) {
    liveData.observe(this, {
        body(it)
    })
}


fun ViewModel.launch(
    coroutineContext: CoroutineContext = EmptyCoroutineContext,
    body: suspend () -> Unit
) {
    viewModelScope.launch(coroutineContext) {
        body()
    }
}


fun Fragment.onMain(d: suspend () -> Unit) {
    lifecycleScope.onMain(d)
}

fun CoroutineScope.onMain(d: suspend () -> Unit) {
    launch(Dispatchers.Main) {
        d()
    }
}

fun UserEntity.toUser(font: Typeface?): User {
    return User(
        id = id,
        name = name,
        family = family,
        img = img,
        lastMessage = lastMessage,
        visibility = if (isOnline == 0) View.INVISIBLE else View.VISIBLE,
        typeface = font,
        time = time

    )
}