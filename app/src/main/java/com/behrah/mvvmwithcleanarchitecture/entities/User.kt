package com.behrah.mvvmwithcleanarchitecture.entities

import android.graphics.Typeface
import android.view.View

data class User(
        var id: Int = 0,
        var name: String? = null,
        var family: String? = null,
        var img: String? = null,
        var lastMessage: String? = null,
        var visibility: Int = View.INVISIBLE,
        var time: String = "",
        var typeface: Typeface?=null
)