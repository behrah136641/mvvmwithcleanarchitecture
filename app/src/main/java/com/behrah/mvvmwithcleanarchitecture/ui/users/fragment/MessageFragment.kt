package com.behrah.mvvmwithcleanarchitecture.ui.users.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.behrah.mvvmwithcleanarchitecture.databinding.MessageFragmentBinding
import com.behrah.mvvmwithcleanarchitecture.extentions.observe
import com.behrah.mvvmwithcleanarchitecture.extentions.onMain
import com.behrah.mvvmwithcleanarchitecture.ui.users.UserViewModel
import com.behrah.mvvmwithcleanarchitecture.ui.users.adapter.RvHorStoUserAdapter
import com.behrah.mvvmwithcleanarchitecture.ui.users.adapter.RvVerMesUserAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MessageFragment : Fragment() {

    private var _binding: MessageFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel: UserViewModel by activityViewModels()

    private val rvVerMesUserAdapter: RvVerMesUserAdapter by lazy {
        RvVerMesUserAdapter()
    }

    private val rvHorStoUserAdapter: RvHorStoUserAdapter by lazy {
        RvHorStoUserAdapter()
    }

    companion object {
        @JvmStatic
        fun newInstance() = MessageFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = MessageFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            viewModel.getAllUsers("").collectLatest {
                rvVerMesUserAdapter.submitData(it)
                //viewModel.getRecentStories()

            }
        }


        observe(viewModel.recentStories){
            rvHorStoUserAdapter.submitList(it)
        }

        binding.rvChatsStories.setHasFixedSize(true)
        binding.rvChatsStories.adapter = rvVerMesUserAdapter

        binding.rvRecentStories.setHasFixedSize(true)
        binding.rvRecentStories.adapter = rvHorStoUserAdapter

        binding.tvMessagesNumber.typeface = viewModel.fontDefault

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}