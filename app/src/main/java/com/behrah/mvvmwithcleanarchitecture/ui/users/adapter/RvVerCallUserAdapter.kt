package com.behrah.mvvmwithcleanarchitecture.ui.users.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.behrah.mvvmwithcleanarchitecture.R
import com.behrah.mvvmwithcleanarchitecture.databinding.ItemCallBinding
import com.behrah.mvvmwithcleanarchitecture.entities.User
import com.behrah.mvvmwithcleanarchitecture.hilt.appModule.GlideApp

class RvVerCallUserAdapter :
    RecyclerView.Adapter<RvVerCallUserAdapter.RvVerUserHolder>() {

    private val asyncListDiffer = AsyncListDiffer(this, object : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem == newItem
        }
    })


    inner class RvVerUserHolder(private val binding: ItemCallBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(user: User?) {

            user?.let {

                binding.tvName.text = String.format("%s %s", user.name, user.family)
                binding.tvTime.text = String.format("%s", user.time.substring(11, 16))
                binding.tvTime.typeface = user.typeface


                GlideApp.with(binding.ivAvatarImage.context)
                    .load(user.img)
                    .placeholder(R.mipmap.ic_launcher_round)
                    .fitCenter()
                    .into(binding.ivAvatarImage)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RvVerUserHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return RvVerUserHolder(ItemCallBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: RvVerUserHolder, position: Int) {
        holder.bind(getItem(position))
    }

    private fun getItem(index: Int) = asyncListDiffer.currentList[index]

    override fun getItemCount(): Int {
        return asyncListDiffer.currentList.size
    }

    fun submitList(user: List<User>) {
        asyncListDiffer.submitList(user)
    }


}