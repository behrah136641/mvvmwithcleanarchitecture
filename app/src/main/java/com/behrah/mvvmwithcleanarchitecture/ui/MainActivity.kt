package com.behrah.mvvmwithcleanarchitecture.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.behrah.domain.utils.Constants.NUM_PAGES_VIEW_PAGER
import com.behrah.mvvmwithcleanarchitecture.R
import com.behrah.mvvmwithcleanarchitecture.databinding.MainActivityBinding
import com.behrah.mvvmwithcleanarchitecture.ui.users.UserViewModel
import com.behrah.mvvmwithcleanarchitecture.ui.users.adapter.ViewPagerFragmentAdapter
import com.behrah.mvvmwithcleanarchitecture.ui.users.fragment.CallFragment
import com.behrah.mvvmwithcleanarchitecture.ui.users.fragment.MessageFragment
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : FragmentActivity() {


    private lateinit var binding: MainActivityBinding
    private val viewModel: UserViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        val pagerAdapter = ViewPagerFragmentAdapter(supportFragmentManager, this.lifecycle)
        pagerAdapter.addFragment(MessageFragment.newInstance())
        pagerAdapter.addFragment(CallFragment.newInstance())

        binding.pager.adapter = pagerAdapter
        TabLayoutMediator(
            binding.tabLayout, binding.pager
        ) { tab, position ->

            if (position == 0)
                tab.setIcon(R.drawable.ic_comment_24dp)
            else
                tab.setIcon(R.drawable.ic_phone)

          //  tab.text = "Tab " + (position + 1)

        }.attach()


        /*binding.ivProfile.setOnClickListener {
            var lang = "en"
            if (viewModel.languageDefault == "en") {
                lang = "fa"
            }

            updateLocale(Locale(lang))
        }*/

    }

    override fun onBackPressed() {
        if (binding.pager.currentItem == 0) {
            super.onBackPressed()
        } else {
            binding.pager.currentItem = binding.pager.currentItem - 1
        }
    }


    private inner class ScreenSlidePagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
        override fun getItemCount(): Int = NUM_PAGES_VIEW_PAGER
        override fun createFragment(position: Int): Fragment {
            return when (position) {
                0 -> MessageFragment.newInstance()
                else -> CallFragment.newInstance()
            }
        }

    }
}


