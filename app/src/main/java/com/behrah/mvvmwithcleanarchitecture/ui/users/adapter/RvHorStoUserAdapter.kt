package com.behrah.mvvmwithcleanarchitecture.ui.users.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.behrah.mvvmwithcleanarchitecture.R
import com.behrah.mvvmwithcleanarchitecture.databinding.AvatarXBinding
import com.behrah.mvvmwithcleanarchitecture.entities.User
import com.behrah.mvvmwithcleanarchitecture.hilt.appModule.GlideApp

class RvHorStoUserAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val asyncListDiffer = AsyncListDiffer(this, object : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem == newItem
        }
    })

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return RvHorUserHolder(AvatarXBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as RvHorUserHolder
        holder.bind(getItem(position))
    }

    private fun getItem(index: Int) = asyncListDiffer.currentList[index]

    override fun getItemCount(): Int {
        return asyncListDiffer.currentList.size
    }

    fun submitList(user: List<User>) {
        asyncListDiffer.submitList(user)
    }

    inner class RvHorUserHolder(private val binding: AvatarXBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(user: User) {
            GlideApp.with(binding.ivAvatarImage.context)
                .load(user.img)
                .placeholder(R.mipmap.ic_launcher_round)
                .fitCenter()
                .into(binding.ivAvatarImage)
        }
    }
}



