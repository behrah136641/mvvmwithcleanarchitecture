package com.behrah.mvvmwithcleanarchitecture.ui.users.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.behrah.mvvmwithcleanarchitecture.R
import com.behrah.mvvmwithcleanarchitecture.databinding.ItemChatsBinding
import com.behrah.mvvmwithcleanarchitecture.entities.User
import com.behrah.mvvmwithcleanarchitecture.hilt.appModule.GlideApp

class RvVerMesUserAdapter : PagingDataAdapter<User, RvVerMesUserAdapter.RvVerUserHolder>(differConfig) {

    companion object {
        val differConfig = object : DiffUtil.ItemCallback<User>() {
            override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem.id == newItem.id
            }
            override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RvVerUserHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return RvVerUserHolder(ItemChatsBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: RvVerUserHolder, position: Int) {
        holder.bind(getItem(position))
    }


    inner class RvVerUserHolder(private val binding: ItemChatsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(user: User?) {

            user?.let {
                binding.tvName.text = String.format("%s %s", user.name, user.family)
                binding.tvLastMessage.text = String.format("%s", user.lastMessage)
                binding.tvTime.text = String.format("%s", user.time.substring(11, 16))
                binding.viewStatus.visibility = user.visibility
                binding.tvTime.typeface = user.typeface


                GlideApp.with(binding.ivAvatarImage.context)
                    .load(user.img)
                    .placeholder(R.mipmap.ic_launcher_round)
                    .fitCenter()
                    .into(binding.ivAvatarImage)
            }
        }
    }


}