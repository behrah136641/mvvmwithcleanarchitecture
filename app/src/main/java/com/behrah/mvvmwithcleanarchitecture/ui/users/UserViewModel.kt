package com.behrah.mvvmwithcleanarchitecture.ui.users

import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.behrah.data.db.UserDao
import com.behrah.domain.contracts.IDefault
import com.behrah.domain.usecases.users.GetAllUsers
import com.behrah.domain.usecases.users.GetRecentCalls
import com.behrah.domain.usecases.users.GetRecentStories
import com.behrah.mvvmwithcleanarchitecture.entities.User
import com.behrah.mvvmwithcleanarchitecture.extentions.launch
import com.behrah.mvvmwithcleanarchitecture.extentions.toUser
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(
    private val getUsers: GetAllUsers,
    private val getRecCalls: GetRecentCalls,
    private val getRecStories: GetRecentStories,
    private val iDefault: IDefault,
    private val userDao: UserDao

) : ViewModel() {

    private val _recentCalls = MutableLiveData<List<User>>()
    val recentCalls: LiveData<List<User>> = _recentCalls

    private val _recentStories = MutableLiveData<List<User>>()
    val recentStories: LiveData<List<User>> = _recentStories


    fun getRecentCalls() {
        launch {
            getRecCalls(Unit).map { list ->
                list.map {
                    it.toUser(fontDefault)
                }
            }.map {
                _recentCalls.value = it
            }

        }
    }

    fun getRecentStories() {
        launch {
            getRecStories(Unit).map { list ->
                _recentStories.value = list.map {
                    it.toUser(fontDefault)
                }
            }

        }
    }

    suspend fun getAllUsers(query: String): Flow<PagingData<User>> {
        return getUsers(query).cachedIn(viewModelScope).map { pagingData ->

            pagingData.map {
                it.toUser(fontDefault)
            }
        }
    }


    var fontDefault = iDefault.getFont()
    var languageDefault = iDefault.getLanguageDefault()


}