package com.behrah.mvvmwithcleanarchitecture.ui.users.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.behrah.mvvmwithcleanarchitecture.databinding.FragmentCallBinding
import com.behrah.mvvmwithcleanarchitecture.extentions.observe
import com.behrah.mvvmwithcleanarchitecture.ui.users.UserViewModel
import com.behrah.mvvmwithcleanarchitecture.ui.users.adapter.RvVerCallUserAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CallFragment : Fragment() {

    private var _binding: FragmentCallBinding? = null
    private val binding get() = _binding!!

    private val viewModel: UserViewModel by activityViewModels()

    private val rvVerCallUserAdapter: RvVerCallUserAdapter by lazy {
        RvVerCallUserAdapter()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observe(viewModel.recentCalls){
            rvVerCallUserAdapter.submitList(it)
        }
        binding.rvCall.setHasFixedSize(true)
        binding.rvCall.adapter = rvVerCallUserAdapter
        viewModel.getRecentCalls()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCallBinding.inflate(inflater, container, false)
        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance() = CallFragment()
    }


}