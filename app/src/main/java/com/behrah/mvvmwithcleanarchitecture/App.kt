package com.behrah.mvvmwithcleanarchitecture

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import com.zeugmasolutions.localehelper.LocaleAwareApplication
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class App : LocaleAwareApplication()

