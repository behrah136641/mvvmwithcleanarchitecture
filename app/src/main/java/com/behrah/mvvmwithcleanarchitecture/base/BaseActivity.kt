package com.behrah.mvvmwithcleanarchitecture.base

import com.zeugmasolutions.localehelper.LocaleAwareCompatActivity

open class BaseActivity : LocaleAwareCompatActivity()