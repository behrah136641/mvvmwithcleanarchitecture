package com.behrah.mvvmwithcleanarchitecture.font

import android.content.Context
import android.graphics.Typeface
import androidx.core.content.res.ResourcesCompat
import com.behrah.domain.contracts.IDefault
import com.behrah.mvvmwithcleanarchitecture.R
import dagger.hilt.android.qualifiers.ApplicationContext
import java.util.*
import javax.inject.Inject

class DefaultImpl @Inject constructor(@ApplicationContext val appContext: Context) : IDefault {
    override fun getFont(): Typeface? {
       return if (getLanguageDefault() == "fa") {
            ResourcesCompat.getFont(appContext, R.font.vazir_light)
        } else {
            ResourcesCompat.getFont(appContext, R.font.lalehzar)
        }
    }

    override fun getLocal(): Locale {
        return Locale.getDefault()
    }

    override fun getLanguageDefault(): String {
        return getLocal().language
    }
}