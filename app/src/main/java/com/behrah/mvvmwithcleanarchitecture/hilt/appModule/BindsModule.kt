package com.behrah.mvvmwithcleanarchitecture.hilt.appModule

import com.behrah.domain.contracts.IDefault
import com.behrah.mvvmwithcleanarchitecture.font.DefaultImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface BindsModule {

    @Binds
    fun bindIDefault(defaultImpl: DefaultImpl): IDefault

}