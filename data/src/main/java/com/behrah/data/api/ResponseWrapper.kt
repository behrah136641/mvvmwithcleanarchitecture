package com.behrah.data.api

import com.google.gson.annotations.SerializedName

data class ResponseWrapper<T>(

    @SerializedName("status")
    val status: ResponseStatus,

    @SerializedName("response")
    val response: T

)

open class ResponseStatus {

    @SerializedName("code")
    open var code: Int = -1

    @SerializedName("message")
    open var message: String = ""
}


data class PagingWrapper<T>(
    val content: T,
    val isFirst: Boolean,
    val isLast: Boolean,
    val totalCount: Int,
    val pageCount: Int,
    val loadKey: Int,
    val pageSize: Int
)