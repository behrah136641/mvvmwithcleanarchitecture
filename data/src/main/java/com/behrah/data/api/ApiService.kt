package com.behrah.data.api


import com.behrah.data.entities.UserData
import retrofit2.http.*


interface ApiService {

/*    @Headers("Accept: application/json")
    @POST("/users/new")
    suspend fun addNewUser(@Body user: UserData): Response

    @GET("/users/list")
    suspend fun getUsers(): UserResponse*/


    @GET("/users/list")
    suspend fun getUsers(
        @Query("query") query: String,
        @Query("page") page: Int,
    ): ResponseWrapper<PagingWrapper<List<UserData>>>
}