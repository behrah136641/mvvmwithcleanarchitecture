package com.behrah.data.hilt.appModules

import android.content.Context
import androidx.room.Room
import com.behrah.data.db.AppDB
import com.behrah.data.db.UserDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class DbModule {

    @Provides
    fun provideDatabase(
        @ApplicationContext appContext: Context
    ): AppDB {
        return Room.databaseBuilder(
            appContext,
            AppDB::class.java,
            "users.db"
        ).fallbackToDestructiveMigration().build()
    }

    @Provides
    fun provideUserDao(db: AppDB): UserDao {
        return db.userDao()
    }


}