package com.behrah.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import androidx.paging.*
import androidx.room.withTransaction
import com.behrah.data.api.ApiService
import com.behrah.data.dataSource.UserPagingSource
import com.behrah.data.db.AppDB
import com.behrah.data.entities.UserData
import com.behrah.data.extension.toUserEntity
import com.behrah.domain.contracts.UserRepository
import com.behrah.domain.entities.UserEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val database: AppDB,
    private val networkService: ApiService

) : UserRepository {

    @ExperimentalPagingApi
    override suspend fun getUsers(query: String): Flow<PagingData<UserEntity>> {

        return Pager(
            config = PagingConfig(enablePlaceholders = false, pageSize = 10),
            pagingSourceFactory = {
                UserPagingSource(query, networkService){
                        database.userDao().insertAll(it)
                }
            }
        ).flow.map { pagingData ->
            pagingData.map {
                it.toUserEntity()
            }
        }

    }

    override suspend fun getRecentStories(): LiveData<List<UserEntity>> {
        return database.userDao().getRecentStories().map { list ->
            list.map {
                it.toUserEntity()
            }
        }
    }

    override suspend fun getRecentCalls(): LiveData<List<UserEntity>> {
        return database.userDao().getRecentCalls().map { list ->
            list.map {
                it.toUserEntity()
            }
        }
    }

    override fun saveUsers(userEntities: UserEntity) {

    }

    override fun deleteAllUsers() {

    }

    override fun deleteUser(users: UserEntity) {

    }

    override fun updateUsers(users: UserEntity) {

    }

}