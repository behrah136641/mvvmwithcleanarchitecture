package com.behrah.data.extension

import com.behrah.data.api.ResponseWrapper
import com.behrah.data.entities.UserData
import com.behrah.domain.entities.UserEntity

inline fun <T> ResponseWrapper<T>.onSuccess(onSuccess: (T) -> Unit) = apply {
    if (status.code in 200..300) {
        onSuccess(response)
    }
}

inline fun ResponseWrapper<*>.onFailure(onFailure: (String) -> Unit) {
    if (status.code >= 400) {
        onFailure(status.message)
    }
}

fun UserEntity.toDataUser(): UserData {
    return UserData(
        id = id,
        name = name,
        family = family,
        img = img,
        lastMessage = lastMessage,
        isOnline = isOnline,
        time = time
    )
}

fun UserData.toUserEntity(): UserEntity {
    return UserEntity(
        id = id,
        name = name,
        family = family,
        img = img,
        lastMessage = lastMessage,
        isOnline = isOnline,
        time = time

    )
}
