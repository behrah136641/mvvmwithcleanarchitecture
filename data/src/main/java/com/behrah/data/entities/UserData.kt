package com.behrah.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "users",primaryKeys = ["id"])
data class UserData(

    @SerializedName("id")
    @ColumnInfo(name = "id")
    var id: Int = 0,

    @SerializedName("first_name")
    @ColumnInfo(name = "name")
    var name: String? = null,

    @SerializedName("last_name")
    @ColumnInfo(name = "family")
    var family: String? = null,

    @SerializedName("img")
    @ColumnInfo(name = "img")
    var img: String? = null,

    @SerializedName("last_message")
    @ColumnInfo(name = "lastMessage")
    var lastMessage: String? = null,

    @SerializedName("is_online")
    @ColumnInfo(name = "isOnline")
    var isOnline: Int? = null,

    @SerializedName("time")
    @ColumnInfo(name = "time")
    var time: String = "",
    )