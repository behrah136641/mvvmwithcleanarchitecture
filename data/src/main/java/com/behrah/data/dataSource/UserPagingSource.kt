package com.behrah.data.dataSource

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import androidx.room.withTransaction
import com.behrah.data.api.ApiService
import com.behrah.data.db.AppDB
import com.behrah.data.entities.UserData
import com.behrah.data.extension.onFailure
import com.behrah.data.extension.onSuccess
import retrofit2.HttpException
import java.io.IOException


class UserPagingSource(
    private val query: String = "",
    private val apiService: ApiService,
    private  val saveUsers: suspend (List<UserData>)->Unit
) : PagingSource<Int, UserData>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, UserData> {

        try {
            val page = params.key ?: 0

            apiService.getUsers(query, page).onSuccess {
                val prevKey = if (page > 0) page - 1 else null

                val nextKey = if (!it.isLast) page + 1 else null

               saveUsers(it.content)

                return LoadResult.Page(
                    data = it.content,
                    prevKey = prevKey,
                    nextKey = nextKey
                )
            }.onFailure {
                return LoadResult.Error(Throwable(it))
            }
            throw Exception("Unexpected error")

        } catch (e: IOException) {
            return LoadResult.Error(e)
        } catch (e: HttpException) {
            return LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, UserData>): Int? {
        return state.anchorPosition?.let {
            state.closestPageToPosition(it)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(it)?.nextKey?.minus(1)
        }
    }
}