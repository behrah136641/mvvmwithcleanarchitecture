package com.behrah.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.behrah.data.entities.UserData

@Database(
    entities = [UserData::class], version = 1, exportSchema = true
)
abstract class AppDB : RoomDatabase() {
    abstract fun userDao(): UserDao
}
