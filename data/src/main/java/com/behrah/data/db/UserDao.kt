package com.behrah.data.db


import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.behrah.data.entities.UserData

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(users: List<UserData>)

    @Query("SELECT * FROM users WHERE name LIKE :query")
    fun getUsers(query: String): PagingSource<Int, UserData>

    @Query("SELECT * FROM users ORDER BY id DESC LIMIT 10")
    fun getRecentStories(): LiveData<List<UserData>>

    @Query("SELECT * FROM users ORDER BY id DESC LIMIT 10")
    fun getRecentCalls(): LiveData<List<UserData>>

    @Query("DELETE FROM users")
    suspend fun clearAll()

    @Query("DELETE FROM users WHERE name LIKE :query")
    suspend fun deleteByQuery(query: String)


}