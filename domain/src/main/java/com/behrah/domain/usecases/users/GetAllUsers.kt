package com.behrah.domain.usecases.users

import androidx.paging.PagingData
import com.behrah.domain.contracts.UserRepository
import com.behrah.domain.entities.UserEntity
import com.behrah.domain.usecases.users.base.BaseUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetAllUsers @Inject constructor(
    private val repository: UserRepository
) : BaseUseCase<String, Flow<PagingData<UserEntity>>>() {
    override suspend fun run(p: String): Flow<PagingData<UserEntity>> {
        return repository.getUsers(p)
    }

}
