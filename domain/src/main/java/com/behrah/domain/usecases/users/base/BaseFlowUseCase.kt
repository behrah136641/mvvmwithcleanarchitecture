package com.behrah.domain.usecases.users.base

import com.behrah.domain.contracts.Result
import kotlinx.coroutines.flow.flow

abstract class BaseFlowUseCase<P, R> {

    protected abstract suspend fun run(p: P): R

    suspend operator fun invoke(p: P) = flow {
        try {
            emit(Result.Success(run(p)))
        } catch (e: Exception) {
            emit(Result.Error<R>(e))
        }
    }


}

fun <T> Result<T>.onSuccess(body: (T) -> Unit) = apply {
    when (this) {
        is Result.Success<T> -> body(data)
    }
}

fun Result<*>.onError(body: (Throwable) -> Unit) {
    when (this) {
        is Result.Error -> body(error)
    }
}