package com.behrah.domain.usecases.users

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.behrah.domain.contracts.UserRepository
import com.behrah.domain.entities.UserEntity
import com.behrah.domain.usecases.users.base.BaseUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetRecentStories @Inject constructor(
    private val repository: UserRepository
) : BaseUseCase<Unit, LiveData<List<UserEntity>>>() {
    override suspend fun run(p: Unit): LiveData<List<UserEntity>> {
        return repository.getRecentStories()
    }

}
