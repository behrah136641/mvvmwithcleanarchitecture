package com.behrah.domain.usecases.users

import com.behrah.domain.contracts.UserRepository
import com.behrah.domain.entities.UserEntity
import com.behrah.domain.usecases.users.base.BaseFlowUseCase
import javax.inject.Inject

class UpdateUsers @Inject constructor(
    private val repository: UserRepository
) : BaseFlowUseCase<UserEntity, Unit>() {
    override suspend fun run(p: UserEntity) {
        repository.updateUsers(p)
    }

}
