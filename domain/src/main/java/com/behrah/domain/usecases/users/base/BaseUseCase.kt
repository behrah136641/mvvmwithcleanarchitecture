package com.behrah.domain.usecases.users.base

abstract class BaseUseCase<P,R> {

    protected abstract suspend fun run(p: P): R

    suspend operator fun invoke(p: P)=run(p)

}