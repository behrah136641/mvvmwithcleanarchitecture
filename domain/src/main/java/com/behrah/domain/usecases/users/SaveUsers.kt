package com.behrah.domain.usecases.users

import com.behrah.domain.contracts.UserRepository
import com.behrah.domain.entities.UserEntity
import com.behrah.domain.usecases.users.base.BaseFlowUseCase
import javax.inject.Inject

class SaveUsers @Inject constructor(
    private val repository: UserRepository
) : BaseFlowUseCase<UserEntity, Unit>() {
    override suspend fun run(p: UserEntity) {
        repository.saveUsers(p)
    }

}
