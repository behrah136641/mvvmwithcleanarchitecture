package com.behrah.domain.usecases.users

import com.behrah.domain.contracts.UserRepository
import com.behrah.domain.usecases.users.base.BaseFlowUseCase
import javax.inject.Inject

class DeleteAllUsers @Inject constructor(
    private val repository: UserRepository
) : BaseFlowUseCase<Unit, Unit>() {

    override suspend fun run(p: Unit) {
        repository.deleteAllUsers()
    }

}