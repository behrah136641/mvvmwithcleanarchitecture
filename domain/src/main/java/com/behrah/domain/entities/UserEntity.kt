package com.behrah.domain.entities

data class UserEntity(
    var id: Int = 0,
    var name: String? = null,
    var family: String? = null,
    var img: String? = null,
    var lastMessage: String? = null,
    var isOnline: Int? = null,
    var time: String = "",
)