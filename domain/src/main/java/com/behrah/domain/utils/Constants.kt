package com.behrah.domain.utils

object Constants {

    const val BASE_URL = "https://api.evareh-print.com"
    const val NUM_PAGES_VIEW_PAGER = 2
    const val STARTING_PAGE_INDEX = 0
}