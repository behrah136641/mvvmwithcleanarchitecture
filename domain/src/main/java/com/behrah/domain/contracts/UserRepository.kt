package com.behrah.domain.contracts

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.behrah.domain.entities.UserEntity
import kotlinx.coroutines.flow.Flow

interface UserRepository {
    suspend fun getUsers(query: String): Flow<PagingData<UserEntity>>
    suspend fun getRecentStories(): LiveData<List<UserEntity>>
    suspend fun getRecentCalls(): LiveData<List<UserEntity>>
    fun saveUsers(userEntities:UserEntity)
    fun deleteAllUsers()
    fun deleteUser(users: UserEntity)
    fun updateUsers(users: UserEntity)
}