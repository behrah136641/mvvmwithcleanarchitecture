
package com.behrah.domain.contracts

import android.graphics.Typeface
import java.util.*

interface IDefault {
    fun getFont(): Typeface?
    fun getLocal(): Locale
    fun getLanguageDefault():String
}